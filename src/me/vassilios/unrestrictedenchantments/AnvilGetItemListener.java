package me.vassilios.unrestrictedenchantments;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class AnvilGetItemListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent event) {
		/*
		 * We firstly check for three conditions: 
		 *     - The event isn't cancelled. 
		 *     - The entity who clicked is a player. 
		 *     - The inventory opened is an anvil.
		 */
		if (!event.isCancelled() && event.getWhoClicked() instanceof Player
				&& event.getInventory() instanceof AnvilInventory) {

			// resultItem is the slot that the player clicked.
			ItemStack resultItem = event.getCurrentItem();

			/*
			 * Slots: 
			 *     - 0: First Item. 
			 *     - 1: Second Item. 
			 *     - 2: Result.
			 * 
			 * If the slot clicked is the result and it's not empty then we proceed.
			 */
			if (event.getSlot() == 2 && !resultItem.getType().equals(Material.AIR)) {
				Player player = (Player) event.getWhoClicked();
				ItemStack airItemStack = new ItemStack(Material.AIR, 1);

				/*
				 * We get the current inventory view so we can remove the two anvil items. 
				 * Then give the player his new item.
				 */
				InventoryView inventoryView = event.getView();
				inventoryView.setItem(0, airItemStack);
				inventoryView.setItem(1, airItemStack);

				player.getInventory().addItem(resultItem);
			}
		}
	}
}
