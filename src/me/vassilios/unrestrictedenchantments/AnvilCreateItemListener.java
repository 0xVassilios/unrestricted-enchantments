package me.vassilios.unrestrictedenchantments;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class AnvilCreateItemListener implements Listener {

	@EventHandler
	public void onAnvil(PrepareAnvilEvent event) {
		ItemStack[] anvilItems = event.getInventory().getContents();
		
		/*
		 * We wait for three conditions:
		 *     - That neither slot in the anvil is empty.
		 *     - That the result slot is empty so we don't override legit items.
		 *     - That the two items are the same so we don't accidentally add different materials.
		 */
		if ((anvilItems[0] != null && anvilItems[1] != null) && event.getResult().getType().equals(Material.AIR)
				&& anvilItems[0].getType().equals(anvilItems[1].getType())) {

			/*
			 * We create a new ItemStack that is the same as the two items in the anvil.
			 * Then we create a Map of all the enchantments from the two items and add them to our ItemStack.
			 */
			ItemStack anvilResult = new ItemStack(anvilItems[0].getType(), 1);
			Map<Enchantment, Integer> enchantments = getEnchantments(anvilItems[0], anvilItems[1]);
			anvilResult.addUnsafeEnchantments(enchantments);

			/*
			 * Afterwards we get the Item Meta and set the name to the text from the Anvil.
			 */
			ItemMeta anvilResultItemMeta = anvilResult.getItemMeta();
			anvilResultItemMeta.setDisplayName(event.getInventory().getRenameText());
			anvilResult.setItemMeta(anvilResultItemMeta);

			event.setResult(anvilResult);
		}
	}

	/*
	 * We get every enchantment from an X amount of items and return a map of them.
	 */
	private Map<Enchantment, Integer> getEnchantments(ItemStack... items) {
		Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();

		for (ItemStack item : items) {
			enchantments.putAll(item.getEnchantments());
		}

		return enchantments;
	}

}
