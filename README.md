# **Unrestricted Enchantments**

**Tested Version**: 1.14.4

**Description**: Allows you to enchant your items with conflicting enchantments such as Fire Protectio and Blast Protection through combining them in the anvil.